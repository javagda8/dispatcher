package com.dp.dispatcher.listeners;

public interface IClientOrderListener {
    void newOrder(int clientId, String what);
}
