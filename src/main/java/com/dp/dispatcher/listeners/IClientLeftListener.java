package com.dp.dispatcher.listeners;

public interface IClientLeftListener {
    void clientLeft(int id);
}
