package com.dp.dispatcher.listeners;

public interface IClientCameListener {
    void clientCame(int id);
}
