package com.dp.dispatcher.listeners;

public interface IClientPaidListener {
    void clientPaid(int id);
}
