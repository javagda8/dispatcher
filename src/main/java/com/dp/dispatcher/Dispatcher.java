package com.dp.dispatcher;

import com.dp.dispatcher.event.IEvent;
import org.apache.commons.lang3.ClassUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Dispatcher {
    private static Dispatcher ourInstance = new Dispatcher();
    private Map<Class<?>, List<Object>> instances = new HashMap<Class<?>, List<Object>>();

    public static Dispatcher getInstance() {
        return ourInstance;
    }

    private Dispatcher() {
    }

    public void registerObject(Object o) {
        List<Class<?>> interfaces = ClassUtils.getAllInterfaces(o.getClass());
        for (Class<?> interfac : interfaces) {
            List<Object> objects = instances.get(interfac);
            if (objects == null) {
                objects = new ArrayList<>();
            }
            objects.add(o);
            instances.put(interfac, objects);
        }
    }

    public void unregisterObject(Object o) {
        // TODO:
    }

    public <T> List<T> getAllObjectsImplementingInterface(Class<T> clas) {
        return (List<T>) instances.get(clas);
    }

    private ExecutorService dispatcherThread = Executors.newSingleThreadExecutor();

    public void dispatch(IEvent e){
        // zdarzenie wysyłam do realizacji
//        dispatcherThread.submit(e);
        dispatcherThread.submit(new Runnable() {
            @Override
            public void run() {
                try{
                    e.run();
                }catch (Exception e){
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }
}
