package com.dp.dispatcher;

import com.dp.dispatcher.listeners.IClientCameListener;
import com.dp.dispatcher.listeners.IClientLeftListener;
import com.dp.dispatcher.listeners.IClientOrderListener;
import com.dp.dispatcher.listeners.IClientPaidListener;

public class Worker implements IClientCameListener, IClientLeftListener, IClientOrderListener, IClientPaidListener {
    private String name;

    public Worker(String name) {
        Dispatcher.getInstance().registerObject(this);
        this.name = name;
    }

    public void clientCame(int id) {
        System.out.println("Zostaję poinformowany o nowym kliencie: " + id + " my name: " + name);
    }

    @Override
    public void clientLeft(int id) {
        //
    }

    @Override
    public void newOrder(int clientId, String what) {
        //
    }

    @Override
    public void clientPaid(int id) {
        //
    }
}
