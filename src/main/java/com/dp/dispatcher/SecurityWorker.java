package com.dp.dispatcher;

import com.dp.dispatcher.listeners.IClientCameListener;
import com.dp.dispatcher.listeners.IClientLeftListener;
import com.dp.dispatcher.listeners.IClientOrderListener;
import com.dp.dispatcher.listeners.IClientPaidListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SecurityWorker implements IClientCameListener, IClientLeftListener, IClientOrderListener, IClientPaidListener {
    private Map<Integer, Boolean> clients = new HashMap<>();
    public SecurityWorker() {
        Dispatcher.getInstance().registerObject(this);
    }

    public void newClient() {
        System.out.println("Klient się pojawił");
    }

    public void clientOrder() {
        System.out.println("Klient zamówił coś");
    }

    public void clientLeft() {
        System.out.println("Klient poszedł");
    }

    @Override
    public void newOrder(int clientId, String what) {
//        throw new NullPointerException();
        System.out.println("Security notified about order: " + what + " " + clientId);
    }

    @Override
    public void clientCame(int id) {
        clients.put(id, false);
    }

    @Override
    public void clientLeft(int id) {
        if(!clients.get(id)){ // nie zapłacił
            System.out.println("Złodziej!");
        }
    }

    @Override
    public void clientPaid(int id) {
        clients.put(id, true);
    }
}
