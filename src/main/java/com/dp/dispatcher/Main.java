package com.dp.dispatcher;

import com.dp.dispatcher.event.EventClientCame;
import com.dp.dispatcher.event.EventClientLeft;
import com.dp.dispatcher.event.EventClientOrder;
import com.dp.dispatcher.event.EventClientPaid;

public class Main {
    public static void main(String[] args) {
        Restaurant r = new Restaurant();

        Dispatcher.getInstance().dispatch(new EventClientCame(123));
        Dispatcher.getInstance().dispatch(new EventClientOrder(123, "cukierki"));
        Dispatcher.getInstance().dispatch(new EventClientPaid(123));
        Dispatcher.getInstance().dispatch(new EventClientLeft(123));

    }
}
