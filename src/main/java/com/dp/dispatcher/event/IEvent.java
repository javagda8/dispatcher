package com.dp.dispatcher.event;

public interface IEvent extends Runnable{
    void run();
}
