package com.dp.dispatcher.event;

import com.dp.dispatcher.Dispatcher;
import com.dp.dispatcher.listeners.IClientOrderListener;

import java.util.List;

public class EventClientOrder implements IEvent {
    private int who;
    private String what;

    public EventClientOrder(int who, String what) {
        this.who = who;
        this.what = what;
    }

    public void run() {
        List<IClientOrderListener> listeners = Dispatcher.getInstance().getAllObjectsImplementingInterface(IClientOrderListener.class);
        for (IClientOrderListener listener : listeners) {
            listener.newOrder(who, what);
        }
    }
}
