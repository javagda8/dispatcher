package com.dp.dispatcher.event;

import com.dp.dispatcher.Dispatcher;
import com.dp.dispatcher.listeners.IClientCameListener;

import java.util.List;

public class EventClientCame implements IEvent {
    private int who;

    public EventClientCame(int who) {
        this.who = who;
    }

    public void run() {

        // prosimy dispatchera o listę obiektów które są zainteresowane tym wydarzeniem
        List<IClientCameListener> objects = Dispatcher.getInstance().getAllObjectsImplementingInterface(IClientCameListener.class);
        // powiadamiamy każdy obiekt.
        for (IClientCameListener listener : objects) {
            listener.clientCame(who);
        }

    }
}
