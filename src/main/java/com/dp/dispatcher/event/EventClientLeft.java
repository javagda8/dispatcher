package com.dp.dispatcher.event;

import com.dp.dispatcher.Dispatcher;
import com.dp.dispatcher.listeners.IClientLeftListener;
import com.dp.dispatcher.listeners.IClientPaidListener;

import java.util.List;

public class EventClientLeft implements IEvent {
    private int who;

    public EventClientLeft(int who) {
        this.who = who;
    }

    public void run() {
        List<IClientLeftListener> listeners = Dispatcher.getInstance().getAllObjectsImplementingInterface(IClientLeftListener.class);
        for (IClientLeftListener listener : listeners) {
            listener.clientLeft(who);
        }
    }
}