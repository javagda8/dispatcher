package com.dp.dispatcher.event;

import com.dp.dispatcher.Dispatcher;
import com.dp.dispatcher.listeners.IClientOrderListener;
import com.dp.dispatcher.listeners.IClientPaidListener;

import java.util.List;

public class EventClientPaid implements IEvent {
    private int who;

    public EventClientPaid(int who) {
        this.who = who;
    }

    public void run() {
        List<IClientPaidListener> listeners = Dispatcher.getInstance().getAllObjectsImplementingInterface(IClientPaidListener.class);
        for (IClientPaidListener listener : listeners) {
            listener.clientPaid(who);
        }
    }
}